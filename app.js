'use strict'
var express = require('express');
var bodyParser = require('body-parser');
//vamos a crear un servicor para recibir peticiones con express
var app = express();
// Archivos de Ruta

var projec_routes = require("./rotes/project.route");

//midelware,es la capa que se ejecuta entes de la peticion para hecer un especie de global.asax
// con body-parser cualquier parametro que llegue lo convierto a un objeto JSON
app.use(bodyParser.urlencoded({ extended : false}));
app.use(bodyParser.json());

// configuracion CORS

// Configurar cabeceras y cors
app.use((req, res, next) => {
    // reemplazar el * por el origen que se quiere permitir
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// configuracion de rutas
app.get("/",(request, response)=>{
    response.status(200).send("<h1>Api REST from nodeJS</h1>");
});

// se agrega a la llamada de rutas el path "api"
app.use("/api", projec_routes);

// app.get("/test",(request,response)=>{
//     response.status(200).send({
//         messaje:"Server status online"
//     });

// });

// app.post("/test/:id",(request, response) =>{

//     // obtener la informacion de parametros 
//     console.log(request.body.nombre);
//     console.log(request.query.web);
//     console.log(request.params.id);

//     response.status(200).send({
//         messaje:"server online POST-Method"
//     })

// })
// exportamos el modulo
module.exports = app;