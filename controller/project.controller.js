'use strict'

var Project = require("../models/project");
var fileSystem = require("fs");
var path = require("path");
// clase con los metodos que se relacionan con la vsita y el modelo
var controller = {
    home: function (req, res) {
        return res.status(200).send({
            message: "soy la home"

        });

    },
    test: function (req, res) {

        return res.status(200).send({

            message: " soy test Rest"
        });
    },

    saveProject: function (req, res) {
        var project = new Project();
        var bodyParams = req.body;
        project.name = bodyParams.name;
        project.descripcion = bodyParams.descripcion;
        project.category = bodyParams.category;
        project.year = bodyParams.year;
        project.langs = bodyParams.langs;
        project.image = null;
        
        project.save((err, projectStored) => {
            if (err) {
                return res.status(500).send({ messaje: "Error inesperado" });

            } else if (!projectStored) {
                return res.status(404).send({ messaje: "No fue posible guardar el project enviado" });
            }
            else {
                return res.status(201).send({ project: projectStored });
            }
        });
    },

    getProject: function (req, res) {
        // obtebemos el parametro de entrada
        var id = req.params.id;
        Project.findById(id, (err, doc) => {
            if (err) return res.status(500).send({ message: "error inesperado al obtener project" });

            if (!doc) return res.status(404).send({ message: "project no encontrado" });

            return res.status(200).send({ project: doc });


        })


    },

    getProjectList: function (rep, res) {

        //  para ordenar los resultados se aplica el sort , para ordenar aceendente se usa el - delante del campo ordenado
        Project.find({"image":{$ne:null}}).sort('-_id').exec((err, docList) => {

            if (err) return res.status(500).send({ message: "Error inesperado al obtener listado" });

            if (!docList) return res.status(404).send({ message: "Error,  no existen datos" });

            return res.status(200).send({ projecs: docList });

        });
    },

    updateProjetById: function (req, res) {

        var id = req.params.id;
        var bodyUpdate = req.body;
        Project.findByIdAndUpdate(id, bodyUpdate, { new: true }, (err, doc) => {
            if (err) return res.status(500).send({ message: "Error inesperado al intentar actualizar el project" });
            if (!doc) return res.status(404).send({ message: "Error project to update not found" });
            return res.status(200).send({ project: doc });

        });

    },
    deleteProjectById: function (req, res) {

        var id = req.params.id;
        Project.findByIdAndDelete(id, (err, doc) => {

            if (err) return rep.status(500).send({ message: "Error al eliminar project" });

            if (!doc) return res.status(404).send({ message: "No se encontro el project a eliminar" });

            return res.status(200).send({ project: doc });

        });

    },

    // trabajaremos con connect-multiparty para el manejo de archivos
    uploadFileById: function (req, res) {
        var allowExtention = ["jpeg", "jpg", "png", "gif"];


        var id = req.params.id;
        var file = req.files;
        if (file) {
            var filename = file.image.path
            if(filename.indexOf("\\") > 0)
            {
                filename = filename.split('\\')[1];
            }
            else{
                filename = filename.split('/')[1];
            }
            var extenFile = filename.split('.')[1];
            if (allowExtention.includes(extenFile)) {
                Project.findByIdAndUpdate(id, { image: filename }, { new: true }, (err, doc) => {
                    if (err) return res.status(500).send({ message: "error al actualizar el nombre del archivo" });
                    if (!doc) return res.status(404).send({ message: "project not found" });
                    return res.status(200).send({ project: doc });

                });
            }
            else {
                // se elimina el archivo desde la carpeta upload, para ello importamos el fs
                fileSystem.unlink(file.image.path, (err) => {
                    if (err) res.status(400).send({ message: err });

                    return res.status(400).send({ message: "Archivo con extencion no permitida" });
                });
            }


        } else {

            return res.status(404).send({ message: "Imagen no subida" });
        }
    },
    // metodo que permite descargar una imagen
    getImage: function (req, res) {
        var file = req.params.image;
       
        var pathFile = "./uploads/" + file;
        fileSystem.exists(pathFile, (exist) => {
            if (exist) {
                return res.sendFile(path.resolve(pathFile));
            }
            else{
                return res.status(404).send({message:"Archivo no encontrado"});
            }

        });

    }

};

module.exports = controller;
