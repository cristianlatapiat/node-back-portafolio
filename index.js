// importamos el modulo de mongoose para conectarnos a la base de datos
var mongoose = require('mongoose');;
// ceamos el servidor que recibira las peticiones
var app = require('./app');

mongoose.Promise = global.Promise;
// como es una promesa puedo ocupar el metoto then para vericicar la conexion
//mongodb+srv://sa:Hq8HBYi60tskY39r@cluster0-lkpls.mongodb.net/test?retryWrites=true&w=majority
// mongoose.connect('mongodb+srv://sa:Hq8HBYi60tskY39r@cluster0-lkpls.mongodb.net/portafolio?retryWrites=true&w=majority').then(() => {
mongoose.connect('mongodb://serverless-mongodb.eastus.azurecontainer.io:27017/portafolios').then(() => {
    console.log("Conexion a base de datos establecida con exito ...");
    var port = process.env.PORT || 5000;
    // creacion del servidor
    app.listen(port, () => {
        console.log("Servidor UP en url http://localhost:" + port)
    })

}).catch(err => console.log(err));




