'use strict'
var mongoose = require("mongoose");
var schema = mongoose.Schema;
var ProjectSchema = schema({
    name:String,
    descripcion:String,
    category:String,
    year:Number,
    langs:String,
    image:String
});

// para poder usar este archivo e importarlo 
// anque este marcado en singular mongoose lo pone en miniusculas y plural
module.exports = mongoose.model('Project',ProjectSchema);