'use strict'
var express = require("express");
var ProjectController = require("../controller/project.controller");
// me permite agregar las rutas al controlador
var router = express.Router();

// configuramos in middlware wue permite hacer cosas antes de ejecutar la peticion
var multiPart = require("connect-multiparty");
var multiparMiddleware = multiPart({ uploadDir: "./uploads" });

router.get("/home", ProjectController.home);
router.post("/test", ProjectController.test);
router.post("/projects", ProjectController.saveProject);
router.get("/projects", ProjectController.getProjectList);
router.get("/projects/:id", ProjectController.getProject);
router.put("/projects/:id", ProjectController.updateProjetById);
router.delete("/projects/:id", ProjectController.deleteProjectById);
// antes de procesar el archivo se acrga en la carpeta uploads, para eso usamos el middleware
router.post("/upload-image/:id",multiparMiddleware , ProjectController.uploadFileById);
router.get("/projects/images/:image",ProjectController.getImage);
// luedo de exportarlo necesito cargalo en el app.js
module.exports = router;
